<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertSouthAmericanCurrencies extends Migration
{
    public function up() {
        DB::table('currencies')->insert([
            [
                'name' => 'Peso Mexicano',
                'symbol' => 'MX'
            ]
        ]);
    }

    public function down() {
        //
    }
}

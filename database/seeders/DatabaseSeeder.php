<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    public function run() {
        $year = date('2022');

    

        // User
        $user = \App\Models\User::create([
            'name' => 'Anibal Laguna',
            'email' => 'alaguna@countul.com',
            'password' => Hash::make('alaguna21'),
        ]);


       /* 
       // User
        $user = \App\Models\User::create([
            'name' => 'Rogelio Cruz',
            'email' => 'rcruz@making.mx',
            'password' => Hash::make('123456'),
        ]);
*/
        // Space
        $space = \App\Models\Space::create([
            'currency_id' => 4,
            'name' => 'Countul SA de CV'
            

        ]);

        $user->spaces()->attach($space);


     
        // Tags
        $tagBills = \App\Models\Tag::create(['space_id' => $space->id, 'name' => '']);
        $tagFood = \App\Models\Tag::create(['space_id' => $space->id, 'name' => 'Comida']);
        $tagTransport = \App\Models\Tag::create(['space_id' => $space->id, 'name' => 'Transporte']);


        for ($i = 1; $i < 12; $i ++) {
            // Income
            \App\Models\Earning::create([
                'space_id' => $space->id,
                'happened_on' => $year . '-' . $i . '-24',
                'description' => 'Pago de Servicios',
                'amount' => 25000
            ]);

            // Bills
            \App\Models\Spending::create([
                'space_id' => $space->id,
                'tag_id' => $tagBills->id,
                'happened_on' => $year . '-' . $i . '-01',
                'description' => 'Teléfono',
                'amount' => 2500
            ]);

            \App\Models\Spending::create([
                'space_id' => $space->id,
                'tag_id' => $tagBills->id,
                'happened_on' => $year . '-' . $i . '-01',
                'description' => 'Gasolina',
                'amount' => 4500
            ]);

            // Food
            for ($j = 0; $j < rand(1, 10); $j ++) {
                \App\Models\Spending::create([
                    'space_id' => $space->id,
                    'tag_id' => $tagFood->id,
                    'happened_on' => $year . '-' . $i . '-' . rand(1, 28),
                    'description' => 'Restaurantes',
                    'amount' => 250 * rand(1, 5)
                ]);
            }

            // Transport
            for ($j = 0; $j < rand(1, 3); $j ++) {
                \App\Models\Spending::create([
                    'space_id' => $space->id,
                    'tag_id' => $tagTransport->id,
                    'happened_on' => $year . '-' . $i . '-' . rand(1, 28),
                    'description' => 'Transporte',
                    'amount' => 1000 * rand(1, 5)
                ]);
            }
        }
    }
}
